package ch.pete.chatmessageparser;

import android.test.AndroidTestCase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Test the MessageParser class.
 * Compare string of examples with the method compareJsonStrings()
 */
public class MessageParserTest extends AndroidTestCase {
    private MessageParser mMessageParser;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mMessageParser = new MessageParser("Unknown Site");
    }

    public void testExample1() throws Exception {
        final String message = "@chris you around?";
        final String jsonString = mMessageParser.parseMessageToJson(message);

        // intentionally use an other Json parser to check, if it still can be read and to show its usage
        // in this exercise.
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());

        final JSONArray mentionsArray = rootObject.getJSONArray("mentions");
        assertEquals("Wrong amount of mentions", 1, mentionsArray.length());
        assertEquals("Wrong mention", "chris", mentionsArray.getString(0));

        final String exampleString = "{ \"mentions\": [ \"chris\" ] }";
        compareJsonStrings(exampleString, jsonString);
    }

    public void testExample2() throws Exception {
        final String message = "Good morning! (megusta) (coffee)";
        final String jsonString = mMessageParser.parseMessageToJson(message);

        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());

        final JSONArray emotionsArray = rootObject.getJSONArray("emoticons");
        assertEquals("Wrong amount of emoticons", 2, emotionsArray.length());
        final Set<String> emotionsSet = jsonArrayToStringSet(emotionsArray);
        assertTrue("Missing 'megusta'", emotionsSet.contains("megusta"));
        assertTrue("Missing 'coffee'", emotionsSet.contains("coffee"));

        final String exampleString = "{ \"emoticons\": [ \"megusta\", \"coffee\" ] }";
        compareJsonStrings(exampleString, jsonString);
    }

    public void testExample3() throws Exception {
        final String message = "Olympics are starting soon; http://www.nbcolympics.com";
        final String jsonString = mMessageParser.parseMessageToJson(message);

        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());

        final JSONArray linksArray = rootObject.getJSONArray("links");
        assertEquals("Wrong amount of links", 1, linksArray.length());

        checkLinkObject("http://www.nbcolympics.com", "NBC Olympics | Home of the 2016 Olympic Games in Rio", linksArray);

        final String exampleString = "{\n" +
                "\"links\": [\n" +
                "\n" +
                "{ \"url\": \"http://www.nbcolympics.com\", \"title\": \"NBC Olympics | Home of the 2016 Olympic Games in Rio\" }\n" +
                "\n" +
                "]\n" +
                "}";
        compareJsonStrings(exampleString, jsonString);
    }

    public void testExample4() throws Exception {
        final String message = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
        final String jsonString = mMessageParser.parseMessageToJson(message);

        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 3, rootObject.length());

        final JSONArray mentionsArray = rootObject.getJSONArray("mentions");
        assertEquals("Wrong amount of mentions", 2, mentionsArray.length());
        final Set<String> mentionsSet = jsonArrayToStringSet(mentionsArray);
        assertTrue("Missing 'bob'", mentionsSet.contains("bob"));
        assertTrue("Missing 'john'", mentionsSet.contains("john"));

        final JSONArray emotionsArray = rootObject.getJSONArray("emoticons");
        assertEquals("Wrong amount of emoticons", 1, emotionsArray.length());
        assertEquals("Wrong emoticon", "success", emotionsArray.getString(0));

        final JSONArray linksArray = rootObject.getJSONArray("links");
        assertEquals("Wrong amount of links", 1, linksArray.length());

        checkLinkObject("https://twitter.com/jdorfman/status/430511497475670016", "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\"", linksArray);

        final String exampleString = "{\n" +
                "\"mentions\": [\n" +
                "\"bob\",\n" +
                "\"john\"\n" +
                "],\n" +
                "\"emoticons\": [\n" +
                "\"success\"\n" +
                "],\n" +
                "\"links\": [\n" +
                "\n" +
                "{ \"url\": \"https://twitter.com/jdorfman/status/430511497475670016\", \"title\": \"Justin Dorfman on Twitter: \\\"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http:\\/\\/t.co\\/7cI6Gjy5pq\\\"\" }\n" +
                "\n" +
                "]\n" +
                "}";
        compareJsonStrings(exampleString, jsonString);
    }

    public void testSentence() throws Exception {
        final String message = "Hi @john (smile1), how are you? I found the interesting page at www.google.com and though you might want to tell @jack (thumbup) about www.atlassian.com. G'day (wave)";
        final String jsonString = mMessageParser.parseMessageToJson(message);

        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 3, rootObject.length());

        final JSONArray mentionsArray = rootObject.getJSONArray("mentions");
        assertEquals("Wrong amount of mentions", 2, mentionsArray.length());
        final Set<String> mentionsSet = jsonArrayToStringSet(mentionsArray);
        assertTrue("Missing 'john'", mentionsSet.contains("john"));
        assertTrue("Missing 'jack'", mentionsSet.contains("jack"));

        final JSONArray emotionsArray = rootObject.getJSONArray("emoticons");
        assertEquals("Wrong amount of emoticons", 3, emotionsArray.length());
        final Set<String> emotionsSet = jsonArrayToStringSet(emotionsArray);
        assertTrue("Missing 'smile1'", emotionsSet.contains("smile1"));
        assertTrue("Missing 'thumbup'", emotionsSet.contains("thumbup"));
        assertTrue("Missing 'wave'", emotionsSet.contains("wave"));

        final JSONArray linksArray = rootObject.getJSONArray("links");
        assertEquals("Wrong amount of links", 2, linksArray.length());

        checkLinkObject("http://www.atlassian.com", "Software Development and Collaboration Tools | Atlassian", linksArray);
        checkLinkObject("http://www.google.com", "Google", linksArray);
    }

    public void testSingleMention() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("@john");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());
        assertEquals("Wrong mention", "john", rootObject.getJSONArray("mentions").getString(0));
    }

    public void testSingleEmoticon() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("(smile)");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());
        assertEquals("Wrong emotion", "smile", rootObject.getJSONArray("emoticons").getString(0));
    }

    public void testEmoticonAfterMention() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("@jack(smile)");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 2, rootObject.length());
        assertEquals("Wrong mention", "jack", rootObject.getJSONArray("mentions").getString(0));
        assertEquals("Wrong mention", "smile", rootObject.getJSONArray("emoticons").getString(0));
    }

    public void testMentionWithUmlaut() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("@jörg");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());
        assertEquals("Wrong mention", "jörg", rootObject.getJSONArray("mentions").getString(0));
    }

    public void testMentionEndWithNonAlphanumericChar() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("@gill$ all good?");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());
        assertEquals("Wrong mention", "gill", rootObject.getJSONArray("mentions").getString(0));
    }

    public void testEmoticonWithUmlaut() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("(jö)");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());
        assertEquals("Wrong mention", "jö", rootObject.getJSONArray("emoticons").getString(0));
    }

    public void testEmoticonWithNonAlphanumbericChar() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("(y$es)");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 0, rootObject.length());
    }

    public void testSingleLink() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("www.sydney.com");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());
        assertEquals("Wrong link url", "http://www.sydney.com", rootObject.getJSONArray("links").getJSONObject(0).getString("url"));
        assertEquals("Wrong link title", "Sydney, Australia - Official Travel & Accommodation Website", rootObject.getJSONArray("links").getJSONObject(0).getString("title"));
    }

    public void testUnknownLink() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("www.somethingnotknown.com");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong amount on root element", 1, rootObject.length());
        assertEquals("Wrong link url", "http://www.somethingnotknown.com", rootObject.getJSONArray("links").getJSONObject(0).getString("url"));
        assertEquals("Wrong link title", "Unknown Site", rootObject.getJSONArray("links").getJSONObject(0).getString("title"));
    }

    public void testNotConformLink() throws Exception {
        final String jsonString = mMessageParser.parseMessageToJson("www.somethingnotknown_com");
        final JSONObject rootObject = new JSONObject(jsonString);
        assertEquals("Wrong values on root element", 0, rootObject.length());
    }

    public void testRuntimeWithoutLink() throws Exception {
        final String message = "Hi @john (smile1), how are you? I found the interesting page and though you might want to tell @jack (thumbup). G'day (wave)";
        final long starTimestamp = System.currentTimeMillis();
        final String jsonString = mMessageParser.parseMessageToJson(message);
        final long duration = System.currentTimeMillis() - starTimestamp;
        assertTrue("runtime too long: " + duration, duration < 10);
    }

    public void testRuntimeWithLinks() throws Exception {
        final String message = "Hi @john (smile1), how are you? I found the interesting page at www.google.com and though you might want to tell @jack (thumbup) about www.atlassian.com. G'day (wave)";
        final long starTimestamp = System.currentTimeMillis();
        final String jsonString = mMessageParser.parseMessageToJson(message);
        final long duration = System.currentTimeMillis() - starTimestamp;
        assertTrue("runtime too long: " + duration, duration < 5000);
    }

    /**
     * check if one of the link objects in linksArray confirms to the given url and title.
     *
     * @param url        to check for
     * @param title      to check for
     * @param linksArray to take object out
     * @throws JSONException
     */
    private void checkLinkObject(String url, String title, JSONArray linksArray) throws JSONException {
        final JSONObject jsonObject = findObjectByStringValue(linksArray, "url", url);
        assertNotNull("links object not found", jsonObject);
        assertEquals("Wrong url", url, jsonObject.getString("url"));
        assertEquals("Wrong title", title, jsonObject.getString("title"));
    }

    /**
     * use Set to ignore order
     *
     * @param jsonArray to convert to set
     * @return the set created from jsonArray
     * @throws JSONException
     */
    private Set<String> jsonArrayToStringSet(JSONArray jsonArray) throws JSONException {
        final Set<String> valuesSet = new HashSet<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            valuesSet.add(jsonArray.getString(i));
        }
        return valuesSet;
    }

    /**
     * @param jsonArray to look for name/value
     * @param name      to look for
     * @param value     to look for
     * @return the found object or fails if not found
     * @throws JSONException on json errors
     */
    private JSONObject findObjectByStringValue(JSONArray jsonArray, String name, String value) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            final JSONObject jsonObject = jsonArray.getJSONObject(i);
            if (jsonObject.has(name) && value.equals(jsonObject.getString(name))) {
                return jsonObject;
            }
        }
        fail("name: " + name + ", value: " + value + " not found");
        return null;
    }

    /**
     * Compare two json strings if they are the same.
     * For simplicity sake, the order of elements in an array must be the same.
     * <p/>
     * Could also be compared with implementing equals()/hashCode() in the classes ParsedMessage and Links
     * but then the same json parser that creates the json string would have to be used.
     *
     * @param expectedJson json string 1 to compare, the expected values
     * @param json         json string 2 to compare
     * @throws JSONException
     */
    private void compareJsonStrings(String expectedJson, String json) throws JSONException {
        final JSONObject rootObject1 = new JSONObject(expectedJson);
        final JSONObject rootObject2 = new JSONObject(json);

        compareJsonRecursive(rootObject1, rootObject2);
    }

    /**
     * needs to use Object because JSONObject and JSONArray do not have a common base class.
     *
     * @param jsonObj1 JSONObject or JSONArray to compare
     * @param jsonObj2 JSONObject or JSONArray to compare
     * @throws JSONException
     */
    private void compareJsonRecursive(Object jsonObj1, Object jsonObj2) throws JSONException {
        if (jsonObj1 instanceof JSONObject) {
            assertTrue("not same type", jsonObj2 instanceof JSONObject);
            final JSONObject jsonObject1 = (JSONObject) jsonObj1;
            final JSONObject jsonObject2 = (JSONObject) jsonObj2;
            assertEquals("Wrong amount of elements", jsonObject1.length(), jsonObject2.length());
            final Iterator<String> keyInterator = jsonObject1.keys();
            while (keyInterator.hasNext()) {
                final String key = keyInterator.next();
                final Object obj = jsonObject1.get(key);
                if (obj instanceof JSONObject || obj instanceof JSONArray) {
                    compareJsonRecursive(obj, jsonObject2.get(key));
                } else {
                    assertEquals("Values not equal", obj, jsonObject2.get(key));
                }
            }
        } else if (jsonObj1 instanceof JSONArray) {
            assertTrue("not same type", jsonObj2 instanceof JSONArray);
            final JSONArray jsonArray1 = (JSONArray) jsonObj1;
            final JSONArray jsonArray2 = (JSONArray) jsonObj2;
            assertEquals("not same array entries count", jsonArray1.length(), jsonArray2.length());
            for (int i = 0; i < jsonArray1.length(); i++) {
                final Object obj = jsonArray1.get(i);
                if (obj instanceof JSONObject || obj instanceof JSONArray) {
                    compareJsonRecursive(obj, ((JSONArray) jsonObj2).get(i));
                } else {
                    assertEquals("values not equals", obj, jsonArray2.get(i));
                }
            }
        } else {
            fail("unknown object type");
        }
    }
}
