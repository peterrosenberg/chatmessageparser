package ch.pete.chatmessageparser.model;

import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class ParsedMessage {
    public static class Link {
        public Link(String url, String title) {
            this.url = url;
            this.title = title;
        }
        private final String url;
        private final String title;
    }
    public final List<String> mentions = new LinkedList<>();
    public final List<String> emoticons = new LinkedList<>();
    public final List<Link> links = new LinkedList<>();

    // do not generate equals()/hashCode() as it is not used, to keep it simple
}
