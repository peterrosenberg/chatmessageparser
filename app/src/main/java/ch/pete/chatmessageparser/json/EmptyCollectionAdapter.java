package ch.pete.chatmessageparser.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Do not encode the collection at all if it is empty.
 */
public class EmptyCollectionAdapter implements JsonSerializer<Collection<?>> {
    @Override
    public JsonElement serialize(Collection<?> src, Type typeOfSrc, JsonSerializationContext context) {
        if (src == null || src.isEmpty()) {
            return null;
        }

        final JsonArray array = new JsonArray();
        for (final Object child : src) {
            final JsonElement element = context.serialize(child);
            array.add(element);
        }

        return array;
    }
}
