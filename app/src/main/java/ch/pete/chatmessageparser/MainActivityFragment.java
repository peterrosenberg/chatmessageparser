package ch.pete.chatmessageparser;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Fragment to show the test page.
 * Not required in this simple app but shows how to use it.
 */
public class MainActivityFragment extends Fragment {
    private MessageParser mMessageParser;
    private TextView mOutput;
    private TextView mInput;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMessageParser = new MessageParser(getActivity().getString(R.string.unknown_title));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mInput = (EditText) rootView.findViewById(R.id.message_input);
        mOutput = (TextView) rootView.findViewById(R.id.output);

        rootView.findViewById(R.id.process).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parseAsync(mInput.getText().toString());
                mInput.setText("");
            }
        });

        return rootView;
    }

    private void parseAsync(final String input) {
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                return mMessageParser.parseMessageToJson(params[0]);
            }

            @Override
            protected void onPostExecute(String result) {
                if (!isAdded()) {
                    // fragment not anymore attached to activity, ignore result
                    return;
                }

                mOutput.setText(result);
            }
        }.execute(input);
    }
}
