package ch.pete.chatmessageparser;

import android.util.Log;
import android.util.Patterns;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.pete.chatmessageparser.json.EmptyCollectionAdapter;
import ch.pete.chatmessageparser.model.ParsedMessage;

/**
 * Parse chat messages and return special content in Json string.
 */
class MessageParser {
    private static final boolean LOG = true;

    private static final String TAG = MessageParser.class.getSimpleName();
    private static final String MENTION_REGEX = "[@](\\w*)";
    private static final String EMOTICONS_REGEX = "[(](\\w{1,15})[)]";

    private final String mUnknownSiteTitle;
    private final Pattern mMentionsPattern;
    private final Pattern mEmoticonsPattern;

    public MessageParser(String unknownSiteTitle) {
        mUnknownSiteTitle = unknownSiteTitle;
        mMentionsPattern = Pattern.compile(MENTION_REGEX);
        mEmoticonsPattern = Pattern.compile(EMOTICONS_REGEX);
    }

    public String parseMessageToJson(final String message) {
        final ParsedMessage parsedMessage = new ParsedMessage();

        final Matcher mentionMatcher = mMentionsPattern.matcher(message);
        while (mentionMatcher.find()) {
            parsedMessage.mentions.add(mentionMatcher.group(1));
        }

        final Matcher emotionMatcher = mEmoticonsPattern.matcher(message);
        while (emotionMatcher.find()) {
            parsedMessage.emoticons.add(emotionMatcher.group(1));
        }

        final List<String> extractedLinks = extractLinks(message);
        for (final String url : extractedLinks) {
            final String normalizedUrl;
            if (!url.toLowerCase().startsWith("http://") && !url.toLowerCase().startsWith("https://")) {
                normalizedUrl = "http://" + url;
            } else {
                normalizedUrl = url;
            }

            String title = null;
            try {
                final Document doc = Jsoup.connect(normalizedUrl).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2").get();
                title = doc.title();
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
                // report to Crashlytics, not done in exercise
                // Crashlytics.getInstance().core.logException(e);
            } catch (MalformedURLException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
                // report to Crashlytics, not done in exercise
                // Crashlytics.getInstance().core.logException(e);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
                // report to Crashlytics, not done in exercise
                // Crashlytics.getInstance().core.logException(e);
            }

            if (title == null) {
                title = mUnknownSiteTitle;
            }
            parsedMessage.links.add(new ParsedMessage.Link(normalizedUrl, title));
        }

        final GsonBuilder gsonBuilder = new GsonBuilder();
        // prevent that empty arrays are visible in the Json string
        gsonBuilder.registerTypeHierarchyAdapter(Collection.class, new EmptyCollectionAdapter());
        final Gson gson = gsonBuilder.create();

        final String gsonString = gson.toJson(parsedMessage);
        if (LOG) {
            Log.d(TAG, gsonString);
        }
        return gsonString;
    }

    private List<String> extractLinks(final String text) {
        final List<String> links = new ArrayList<>();
        final Matcher matcher = Patterns.WEB_URL.matcher(text);
        while (matcher.find()) {
            final String url = matcher.group();
            links.add(url);
        }
        return links;
    }
}
