Repository of Chat Message Parser
=================================

This is a test app for the class MessageParser.

MessageParser takes a chat message string and returns a JSON string containing information about its contents. Special content to look for includes:

1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character.
2. Emoticons - Alphanumeric strings, no longer than 15 characters, contained in parenthesis. Anything matching this format is an emoticon.
3. Links - Any URLs contained in the message, along with the page's title.

Example
-------
Input:
```
#!text
"Good morning @chris! (megusta) (coffee), see www.google.com"
```

Result:
```
#!text
{"emoticons":["megusta","coffee"],
"links":[{"title":"Google","url":"http://www.google.com"}],
"mentions":["chris"]}
```